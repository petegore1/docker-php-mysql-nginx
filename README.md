## Off-Shadow

### Pré-requis
* Installer Docker en local
* Installer Docker-Compose en local

### Provisioning
* Renseigner ce qu'il faut dans le fichier `.env`
* Lancer `sh ./run.sh`
* Si les certificats existent déjà, lancer `docker-compose up -d --build`
