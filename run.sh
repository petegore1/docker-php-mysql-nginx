#!/usr/bin/env bash

# Run this file to launch docker-compose and initialize HTTPS on the server

sudo chmod +x ./docker/nginx/script/init-letsencrypt.sh
./docker/nginx/script/init-letsencrypt.sh && docker-compose up -d