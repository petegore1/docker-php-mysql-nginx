#!/usr/bin/env

echo '### REMOVING OLD BACKUPS'

# Hourly > not older than 24 hours
if [ $2 = "hourly" ]
then
    sudo find /root/backup/ -type f -mtime +1 -name '*hourly.sql.gz' -execdir rm -- '{}' \;
fi

# Daily > between 24h and J-8
if [ $2 = "daily" ]
then
    find /root/backup/ -type f -mtime -1 -name '*daily.sql.gz' -execdir rm -- '{}' \;
    find /root/backup/ -type f -mtime +8 -name '*daily.sql.gz' -execdir rm -- '{}' \;
fi

# Weekly > between J-9 and J-183 (6 months)
if [ $2 = "weekly" ]
then
    find /root/backup/ -type f -mtime -8 -name '*weekly.sql.gz' -execdir rm -- '{}' \;
    find /root/backup/ -type f -mtime +183 -name '*weekly.sql.gz' -execdir rm -- '{}' \;
fi



DATE=`date +%y%m%d%H%M`

echo "BACKUP - $DATE - BEGIN"

echo '### LISTING DATABASES'
LISTEBDD=$( echo 'show databases' | mysql -uroot -p$MYSQL_ROOT_PASSWORD )

echo $(echo 'show databases' | mysql -uroot -p$MYSQL_ROOT_PASSWORD)

#on boucle sur chaque dossier (for découpe automatiquement par l'espace)
echo '### SAVING DATABASES'
for SQL in $LISTEBDD
do

    if [ $SQL != "Database" ] && [ $SQL != "information_schema" ] && [ $SQL != "mysql" ] && [ $SQL != "performance_schema" ]; then

        #echo $SQL
        echo "### SAVING $SQL"
        mysqldump -uroot -p$MYSQL_ROOT_PASSWORD $SQL | gzip > $1/$SQL"_backup_"$DATE"_"$2.sql.gz

    fi

done

echo "BACKUP - $DATE - END"